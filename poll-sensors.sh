PI1=$(ssh -i ~/.ssh/lf-rsa pi@192.168.1.102 "cat /sys/bus/w1/devices/28-00000a4242f1/w1_slave")
PI2=$(ssh -i ~/.ssh/lf-rsa pi@192.168.1.103 "cat /sys/bus/w1/devices/28-00000a422039/w1_slave")
PI3=$(ssh -i ~/.ssh/lf-rsa pi@192.168.1.104 "cat /sys/bus/w1/devices/28-00000a4239f8/w1_slave")
PI4=$(ssh -i ~/.ssh/lf-rsa pi@192.168.1.105 "cat /sys/bus/w1/devices/28-00000a42407f/w1_slave")
PI1=`echo $PI1|cut -d '=' -f3`
PI2=`echo $PI2|cut -d '=' -f3`
PI3=`echo $PI3|cut -d '=' -f3`
PI4=`echo $PI4|cut -d '=' -f3`
echo "sensor=pi1 t=$PI1" >> sensordata.log
echo "sensor=pi2 t=$PI2" >> sensordata.log
echo "sensor=pi3 t=$PI3" >> sensordata.log
echo "sensor=pi4 t=$PI4" >> sensordata.log

