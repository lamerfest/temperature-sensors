# Temperature sensors

Designed to work with specific hardware and SSH keys. You need to make updates to use different hardware.
* Set your SSH keys (~/.ssh/lf_rsa)
* Update the sensor specific paths in poll_sensors.sh (28-XXXXXXX..)

Script poll_sensors.sh writes output to sensor-data.log n format: sensor=X; t=XXXXX